# Install libraries
```yarn add -D @nrwl/web @nrwl/react```

## Add app

```npx nx g @nrwl/react:app user-app --js=true --unitTestRunner=none --e2eTestRunner=none```

```npx nx g @nrwl/react:app product-app --js=true --unitTestRunner=none --e2eTestRunner=none```

```npx nx g @nrwl/react:app cart-app --js=true --unitTestRunner=none --e2eTestRunner=none```

## Run & build app

```npx nx run-many --parallel --targets=serve,build --all```

```npx nx serve user-app```

```npx nx build user-app```

