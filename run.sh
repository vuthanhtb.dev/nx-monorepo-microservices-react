docker build -f Dockerfile.user -t vuvanthanhtb/nx-user-service:latest . &&
docker build -f Dockerfile.product -t vuvanthanhtb/nx-product-service:latest . &&
docker build -f Dockerfile.cart -t vuvanthanhtb/nx-cart-service:latest . &&

docker run -d -p 4200:80 --name nx-user-service vuvanthanhtb/nx-user-service:latest &&
docker run -d -p 4100:80 --name nx-product-service vuvanthanhtb/nx-product-service:latest &&
docker run -d -p 4000:80 --name nx-cart-service vuvanthanhtb/nx-cart-service:latest
